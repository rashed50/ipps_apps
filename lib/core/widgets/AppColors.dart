import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
 class AppColors{
    static Color cursorColor = Colors.black;
    static  Color appBarColor = Colors.blue;
    static  Color appBarTitleColor = Colors.white;
    static Color bodyTextColor = Colors.black;
    static Color buttonColor = Colors.blue;
    static Color buttonSplashColor = Colors.deepOrange;
    static Color buttonIconColor = Colors.white;
    static Color bodyBackgroundColor = const Color(0xffe0e0e0);
    static Color bottomNevigationBarColor = Colors.blue;
 }


