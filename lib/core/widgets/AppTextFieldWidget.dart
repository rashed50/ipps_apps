
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget AppTextFieldWidget(
    {
      Color cursorColor = Colors.black,
      Color containerColor ,
      bool obscureText = false,
      IconData prefixIcon,
      IconData suffixIcon,
      IconData icon,
      bool filled = true,
      Color fillColor = const Color(0xffd5d5d5),
      String labelText,
      Color hintColor = const Color(0xff5f5a5a),
      Color labelColor =  Colors.black87,
      Color enableBorderColor = Colors.blue,
      Color focusBorderColor =  Colors.deepOrangeAccent,
      Function(String) onSave,
      double horizontal_pading = 10,
      double vartical_pading = 0,
      String hintText,
      double padding = 0.0,
      double padding_right,
      double padding_left,
      double width,
      double height,
      bool enable = true,
      int maxLines,
      int minLines = 1,
      TextEditingController controller,
      TextInputType textInputType = TextInputType.text
    }
    ) {
  return Container(
    margin: EdgeInsets.fromLTRB(20, 7, 20, 7),
    color: maxLines != null ? fillColor : containerColor,
    height: maxLines != null ? maxLines * 80.0 : height,
    padding: EdgeInsets.only(
        left: padding_left ?? padding, right: padding_right ?? padding),
    child: TextField(
      controller: controller,
      maxLines: maxLines,
      minLines: minLines,
      enabled: enable,
      keyboardType: textInputType,
      cursorColor: cursorColor,
      style: TextStyle(color: Colors.black,
      fontSize: 15,
        fontWeight: FontWeight.w400,
      ),
      obscureText: false,
      decoration: InputDecoration(
        prefixIcon: prefixIcon != null
            ? Icon(
          prefixIcon,
          size: 28,
          color: Colors.blue,
        )
            : null,
        suffixIcon: suffixIcon != null
            ? Icon(
          suffixIcon,
          size: 28,
          color: Colors.blue,
        )
            : null,
        icon: icon != null
            ? Icon(icon,
          size: 32,
          color: Colors.blue,
        )
            : null,
        filled: filled,
        fillColor: fillColor,
        hintText: hintText,
        hintStyle: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 14,
          color: hintColor,
        ),
        labelText: labelText,
        labelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 18,
          color: labelColor,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(7)),
          borderSide: BorderSide(color: focusBorderColor,width: 2),
        ),
        contentPadding: EdgeInsets.symmetric(
            vertical: vartical_pading, horizontal: horizontal_pading),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            borderSide: BorderSide(color: enableBorderColor,width: 1)),
        border: OutlineInputBorder(borderSide: BorderSide(color: enableBorderColor,width:1)),
      ),
    ),
  );
}