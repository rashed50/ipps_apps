import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipms_app/core/Network/NetworkInfo.dart';
import 'file:///G:/Flutter%20Project/ipms/ipps_apps/ipps_apps/lib/data/Repository/Repo/CompanyProfileRepository.dart';
import 'package:ipms_app/data/Repository/Remote/CompanyProfileRemoteRepository.dart';
import 'package:ipms_app/presentation/Routes/AppRoutes.dart';
import 'package:ipms_app/presentation/Screen/Customer/AddCustomer/AddCustomerScreenController.dart';
import 'package:ipms_app/presentation/Screen/Customer/CustomerScreenController.dart';
import 'package:ipms_app/presentation/Screen/EmployeeInfo/EmployeeInfoScreenController.dart';
import 'package:ipms_app/presentation/Screen/HomePage/HomeScreenController.dart';
import 'package:ipms_app/presentation/Screen/Income/IncomeScreenController.dart';
import 'package:ipms_app/presentation/Screen/LogIn/LogInScreenController.dart';
import 'package:ipms_app/presentation/Screen/LogOut/LogOutScreenController.dart';
import 'package:ipms_app/presentation/Screen/MyProfile/MyProfileScreenController.dart';
import 'package:ipms_app/presentation/Screen/Payment/PaymentScreenController.dart';
import 'package:ipms_app/presentation/Screen/Purchase/PurchaseScreenController.dart';
import 'package:ipms_app/presentation/Screen/Sales/SalesScreenController.dart';
import 'package:ipms_app/presentation/Screen/SubContact/SubContactScreenController.dart';
import 'package:ipms_app/presentation/Screen/Vendor/VendorScreenController.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.put(HomeScreenController(),);
    Get.put(CustomerScreenController(),);
    Get.put( IncomeScreenController(),);
    Get.put( LogOutScreenController(),);
    Get.put( MyProfileScreenController(),);
    Get.put( PaymentScreenController(),);
    Get.put( PurchaseScreenController(),);
    Get.put( SalesScreenController(),);
    Get.put( SubContactScreenController(),);
    Get.put( VendorScreenController(),);
    Get.put(LogInScreenController(),);
    Get.put(EmployeeScreenController(),);
    Get.put(AddCustomerScreenController(),);
    Get.put(CompanyProfileRemoteRepository(),);
  //  Get.put(CompanyProfileRepository());
   // Get.put(NetworkInfoController());
    //Get.put( ,);
    return GetMaterialApp(
      getPages: AppRoutes.appRoutesList(),
      initialRoute: AppRoutes.LOGIN_SCREEN,
    );
  }
}
