import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipms_app/presentation/Routes/AppRoutes.dart';
class SubContractScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      bottomNavigationBar: Container(
        color: Colors.blue,
        height: 25,
        width: Get.width,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RaisedButton.icon(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                  onPressed: (){
                    Get.toNamed(AppRoutes.SUBCONTRACTOR_SCREEN);
                  },
                  elevation: 15,
                  color: Colors.blue,
                  splashColor: Colors.red,
                  focusColor: Colors.deepOrange,
                  icon: Icon(Icons.add,color: Colors.white,),
                  label: Text('Add Sub-Contractor',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),),
                RaisedButton.icon(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                  onPressed: (){
                    Get.toNamed(AppRoutes.SUBCONTRACTPAYMENT_SCREEN);
                  },
                  elevation: 15,
                  color: Colors.blue,
                  splashColor: Colors.red,
                  icon: Icon(Icons.list_alt,color: Colors.white,),
                  label: Text('Sub-Contract Payment',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),),
                RaisedButton.icon(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                  onPressed: (){

                  },
                  elevation: 15,
                  color: Colors.blue,
                  splashColor: Colors.red,
                  icon: Icon(Icons.list_alt,color: Colors.white,),
                  label: Text('Sub-Contractor List',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),),
                RaisedButton.icon(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                  onPressed: (){
                    Get.toNamed(AppRoutes.SUBCONTRACTBOOKING_SCREEN);
                  },
                  elevation: 15,
                  color: Colors.blue,
                  splashColor: Colors.red,
                  icon: Icon(Icons.add_business,color: Colors.white,),
                  label: Text('Sub-Contract Booking',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
