import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipms_app/core/widgets/AppTextFieldWidget.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_image_picker/form_builder_image_picker.dart';
class SubContractorScreeen extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  List<String> CountryOptions = ['Bangladesh','India','Pakisthan','Sri-Lanka','Englend'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('SubContract Payment',
          style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.w800
          ),),
      ),
      bottomNavigationBar: Container(
        color: Colors.blue,
        height: 25,
        width: Get.width,
      ),
      backgroundColor: Colors.grey[200],
      body: FormBuilder(
        key: _formKey,
        child: SafeArea(
          child:SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:<Widget> [
                SizedBox(height: 25,),
                Center(
                  child: Text('Provide the following information',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                      letterSpacing: .8,
                    ),),
                ),
                SizedBox(height: 25,),
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Column(
                    children: <Widget>[
                      AppTextFieldWidget(hintText:'Input Your Name',labelText:'Name',prefixIcon: Icons.account_circle ),
                      AppTextFieldWidget(hintText: 'Input Your Phone NO',labelText: 'Contact Number',prefixIcon: Icons.call),
                      AppTextFieldWidget(hintText: 'Input Father\'s Name', labelText: 'Father\'s Name',prefixIcon:Icons.account_box),
                    ],
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 30,right: 20),
                    child: Text('Address',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600
                      ),)),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                  padding: EdgeInsets.fromLTRB(20,10,20,80),

                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.blue,width: 2),
                    borderRadius: BorderRadius.circular(8,),
                  ),
                  child: Column(
                    children:<Widget> [
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 10),
                        child: FormBuilderDropdown(
                          name: 'country',
                          decoration: InputDecoration(
                            labelText: 'Country Name',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),

                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Customer\'s Country'),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: CountryOptions
                              .map((country) => DropdownMenuItem(
                            value: country,
                            child: Text('$country'),
                          ))
                              .toList(),
                        ),
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 10),
                        child: FormBuilderDropdown(
                          name: 'devision',
                          decoration: InputDecoration(
                            labelText: 'Devision Name',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),
                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Devision'),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: CountryOptions
                              .map((devision) => DropdownMenuItem(
                            value: devision,
                            child: Text('$devision'),
                          ))
                              .toList(),
                        ),
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 10),
                        child: FormBuilderDropdown(
                          name: 'district',
                          decoration: InputDecoration(
                            labelText: 'District Name',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),
                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select District'),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: CountryOptions
                              .map((district) => DropdownMenuItem(
                            value: district,
                            child: Text('$district'),
                          ))
                              .toList(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical:5),
                        child: TextField(
                          maxLines: 3,
                          minLines: 2,
                          decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(7)),
                                borderSide: BorderSide(color: Colors.blue,width: 1)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(7)),
                                borderSide: BorderSide(color: Colors.deepOrangeAccent,width: 2)),
                            hintText: 'Enter Your Address Details' ,
                            hintStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: const Color(0xff5f5a5a),
                            ),
                            labelText: 'Details of Address',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            // icon: Icon(Icons.home,color: Colors.blue,)

                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              Container(

                padding: EdgeInsets.only(bottom: 20),
                margin: EdgeInsets.only(left: 20,right: 20),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.blue,width: 2),
                    borderRadius: BorderRadius.circular(8,)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children:<Widget> [
                    Container(
                      width: 150,
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                      child: FormBuilderImagePicker(name: 'photos',
                        decoration:  InputDecoration(labelText: 'Choose Photo',
                            labelStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.black
                            )),
                        maxImages: 1,
                      ),
                    ),
                    Container(padding: EdgeInsets.only(top:20),
                        child: Container(width: 2,height: 100,color:Colors.blue[600])),
                    Container(
                      width: 150,
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: 10,right: 10),
                      child: FormBuilderImagePicker(name: 'NID',
                        decoration: const InputDecoration(labelText: 'Choose NID Image',
                            labelStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.black
                            )),
                        maxImages: 1,
                      ),
                    ),
                  ],
                ),
              ),//photo picker
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children:<Widget> [

                    Container(
                      margin: EdgeInsets.fromLTRB(10,60,10,20),
                      child: RaisedButton.icon(
                        padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                        onPressed: (){},
                        elevation: 10,
                        color: Colors.blue,
                        splashColor: Colors.red,
                        icon: Icon(Icons.assignment_turned_in_rounded,color: Colors.white,),
                        label: Text('SUBMIT',
                          style: TextStyle(
                            fontWeight: FontWeight.w800,
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),),),
                  ],
                ),
                SizedBox(height: 100,)
              ],
            ),
          )
          ,
        ),
      ),
    );

  }
}


