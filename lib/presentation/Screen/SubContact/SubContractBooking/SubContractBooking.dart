import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:ipms_app/core/widgets/AppColors.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import 'SubContractBookingController.dart';
class SubContractBookingScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  int i=1;
  int x;
  List<String> CountryOptions = ['Bangladesh','India','Pakisthan','Sri-Lanka','Englend'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.appBarColor,
        title: Text('Sub-Contract Booking',
          style: TextStyle(
              fontSize: 20,
              color: AppColors.appBarTitleColor,
              fontWeight: FontWeight.w800
          ),
        ),
        centerTitle: true,
      ),
      bottomNavigationBar: Container(
        color: AppColors.bottomNevigationBarColor,
        height: 25,
        width: Get.width,
      ),
      backgroundColor:AppColors.bodyBackgroundColor,
      body: FormBuilder(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:<Widget> [
              SizedBox(height: 25,),
              Center(
                child: Text('Input Order Details',
                  style: TextStyle(
                    color: AppColors.bodyTextColor,
                    fontSize: 17,
                    fontWeight: FontWeight.w800,
                    letterSpacing: 0,
                  ),),
              ),
              Container(
                //width: Get.width,
                margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                padding: EdgeInsets.fromLTRB(20,10,20,80),

                decoration: BoxDecoration(
                  border: Border.all(color: Colors.blue,width: 2),
                  borderRadius: BorderRadius.circular(8,),
                ),
                child: Column(
                  children:<Widget> [
                    Container(
                      margin:EdgeInsets.symmetric(vertical: 10),
                      child: FormBuilderDropdown(
                        name: 'devision',
                        decoration: InputDecoration(
                          labelText: 'Party Name',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black87,
                          ),
                          filled: true,
                          fillColor: Colors.grey[300],
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue,width: 1)
                          ),
                          focusColor: Colors.blue,
                        ),
                        // initialValue: 'Male',
                        allowClear: true,
                        hint: Text('Select Party'),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: CountryOptions
                            .map((party) => DropdownMenuItem(
                          value: party,
                          child: Text('$party'),
                        ))
                            .toList(),
                      ),
                    ),
                    Container(
                      margin:EdgeInsets.symmetric(vertical: 10),
                      child: FormBuilderDropdown(
                        name: 'Catagory',
                        decoration: InputDecoration(
                          labelText: 'Catagory',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black87,
                          ),
                          filled: true,
                          fillColor: Colors.grey[300],
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue,width: 1)
                          ),
                          focusColor: Colors.blue,
                        ),
                        // initialValue: 'Male',
                        allowClear: true,
                        hint: Text('Select Catagory'),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: CountryOptions
                            .map((catagory) => DropdownMenuItem(
                          value: catagory,
                          child: Text('$catagory'),
                        ))
                            .toList(),
                      ),
                    ),
                    Container(
                      margin:EdgeInsets.symmetric(vertical: 10),
                      child: FormBuilderDropdown(
                        name: 'brand',
                        decoration: InputDecoration(
                          labelText: 'Brand',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black87,
                          ),
                          filled: true,
                          fillColor: Colors.grey[300],
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue,width: 1)
                          ),

                          focusColor: Colors.blue,
                        ),
                        // initialValue: 'Male',
                        allowClear: true,
                        hint: Text('Select Brand'),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: CountryOptions
                            .map((brand) => DropdownMenuItem(
                          value: brand,
                          child: Text('$brand'),
                        ))
                            .toList(),
                      ),
                    ),
                    Container(
                      margin:EdgeInsets.symmetric(vertical: 10),
                      child: FormBuilderDropdown(
                        name: 'Size',
                        decoration: InputDecoration(
                          labelText: 'Size',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black87,
                          ),
                          filled: true,
                          fillColor: Colors.grey[300],
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue,width: 1)
                          ),
                          focusColor: Colors.blue,
                        ),
                        // initialValue: 'Male',
                        allowClear: true,
                        hint: Text('Select Size'),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: CountryOptions
                            .map((size) => DropdownMenuItem(
                          value: size,
                          child: Text('$size'),
                        ))
                            .toList(),
                      ),
                    ),
                    Container(
                      margin:EdgeInsets.symmetric(vertical: 10),
                      child: FormBuilderDropdown(
                        name: 'Weight',
                        decoration: InputDecoration(
                          labelText: 'Weight',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black87,
                          ),
                          filled: true,
                          fillColor: Colors.grey[300],
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue,width: 1)
                          ),
                          focusColor: Colors.blue,
                        ),
                        // initialValue: 'Male',
                        allowClear: true,
                        hint: Text('Select Product\'s Weight'),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: CountryOptions
                            .map((Weight) => DropdownMenuItem(
                          value: Weight,
                          child: Text('$Weight'),
                        ))
                            .toList(),
                      ),
                    ),
                    Container(
                      margin:EdgeInsets.symmetric(vertical: 10),
                      child: FormBuilderDropdown(
                        name: 'netSize',
                        decoration: InputDecoration(
                          labelText: 'Net Size',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black87,
                          ),
                          filled: true,
                          fillColor: Colors.grey[300],
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue,width: 1)
                          ),

                          focusColor: Colors.blue,
                        ),
                        // initialValue: 'Male',
                        allowClear: true,
                        hint: Text('Select Net Size'),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: CountryOptions
                            .map((netSize) => DropdownMenuItem(
                          value: netSize,
                          child: Text('$netSize'),
                        ))
                            .toList(),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical:5),
                      child: TextField(

                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(7)),
                              borderSide: BorderSide(color: Colors.blue,width: 1)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(7)),
                              borderSide: BorderSide(color: Colors.deepOrangeAccent,width: 2)),
                          hintText: 'Input Total Number Of Item' ,
                          hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: const Color(0xff5f5a5a),
                          ),
                          labelText: 'Quantity',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black87,
                          ),
                          prefixIcon: Icon(Icons.auto_awesome_motion,color: Colors.blue,)

                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10,60,10,20),
                child: RaisedButton.icon(
                  padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                  onPressed: (){
                    i += 1;
                    print(i);
                  },
                  elevation: 10,
                  color: Colors.blue,
                  splashColor: Colors.red,
                  icon: Icon(Icons.assignment_turned_in_rounded,color: Colors.white,),
                  label: Text('Add to Cart',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),),),
              SizedBox(height: 100,),
              GetBuilder<SubContractBookingController>(
                builder: (_obj){
                return ListView.builder(
                    itemCount: i,
                    itemBuilder:(context,i){
                     List _customer = CountryOptions;
                      return Container();
                    });
              }
              )

            ],
          ),
        ),
      ),
    );

  }
}