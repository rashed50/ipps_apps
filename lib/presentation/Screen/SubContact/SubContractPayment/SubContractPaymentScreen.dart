import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipms_app/core/widgets/AppTextFieldWidget.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_image_picker/form_builder_image_picker.dart';
class SubContractPaymentScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  List<String> CountryOptions = ['Bangladesh','India','Pakisthan','Sri-Lanka','Englend'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SubContract Payment',
          style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.w800
          ),),
      ),
      bottomNavigationBar: Container(
        color: Colors.blue,
        height: 25,
        width: Get.width,
      ),
      backgroundColor: Colors.grey[200],
      body: FormBuilder(
        key: _formKey,
        child: SafeArea(
          child:SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget> [
                SizedBox(height: 25,),
                Center(
                  child: Text('Insert Payment Details',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.w800,
                      letterSpacing: .8,
                    ),),
                ),
                SizedBox(height: 5,),
                Container(
                  width: Get.width,
                  margin: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                  padding: EdgeInsets.fromLTRB(0,20,0,30),

                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.blue,width: 2,),
                    borderRadius: BorderRadius.circular(8,),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                        child: FormBuilderDropdown(
                          name: 'contractorName',
                          decoration: InputDecoration(
                            labelText: 'Name Of The Contractor',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),

                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Contractor'),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: CountryOptions
                              .map((contructorName) => DropdownMenuItem(
                            value: contructorName,
                            child: Text('$contructorName'),
                          ))
                              .toList(),
                        ),
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                        child: FormBuilderDropdown(
                          name: 'payType',
                          decoration: InputDecoration(
                            labelText: 'Payment Type',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),

                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Payment Type'),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: CountryOptions
                              .map((payType) => DropdownMenuItem(
                            value: payType,
                            child: Text('$payType'),
                          ))
                              .toList(),
                        ),
                      ),
                      Container(
                          child: AppTextFieldWidget(hintText: 'Input Amount',labelText:'Amount',prefixIcon: Icons.attach_money_outlined )
                      ),
                      Container(
                          child: AppTextFieldWidget(hintText: 'Input Discount Amount',labelText:'Discount',prefixIcon: Icons.attach_money)
                      ),
                      Container(
                          child: AppTextFieldWidget(hintText: 'Input Voucher Number',labelText:'Voucher NO',prefixIcon: Icons.format_list_bulleted_outlined)
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                        child: FormBuilderDateTimePicker(
                          name: 'paymentDate',
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.access_time,size:30,color: Colors.blue,),
                              hintText: 'Input Date & Time',
                              hintStyle: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                  color: const Color(0xff5f5a5a)
                              ),
                                labelText: 'Payment Date & Time',
                                labelStyle: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                  color: Colors.black87,
                                ),
                              fillColor: const Color(0xffd5d5d5),
                              filled:true,
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 2,color: Colors.blue)
                              ),
                            ),
                            style: TextStyle(
                              color:Colors.black87
                            ),
                        ),
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                        child: FormBuilderDropdown(
                          name: 'currencyName',
                          decoration: InputDecoration(
                            labelText: 'Currency',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),

                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Currency'),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: CountryOptions
                              .map((currencyName) => DropdownMenuItem(
                            value: currencyName,
                            child: Text('$currencyName'),
                          ))
                              .toList(),
                        ),
                      ),
                      SizedBox(height: 40,)
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10,60,10,20),
                  child: RaisedButton.icon(
                    padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                    onPressed: (){},
                    elevation: 10,
                    color: Colors.blue,
                    splashColor: Colors.red,
                    icon: Icon(Icons.assignment_turned_in_rounded,color: Colors.white,),
                    label: Text('SUBMIT',
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),),),
                SizedBox(height: 50,)
              ],
            ),
          )
          ,
        ),
      ),
    );

  }
}


