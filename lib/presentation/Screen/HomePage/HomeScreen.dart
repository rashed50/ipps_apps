import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipms_app/presentation/Routes/AppRoutes.dart';
import 'package:ipms_app/presentation/Screen/Customer/CustomerScreen.dart';
import 'package:ipms_app/presentation/Screen/Drawer/DrawerMenu.dart';
import 'package:ipms_app/presentation/Screen/EmployeeInfo/EmployeeInfoScreen.dart';
import 'package:ipms_app/presentation/Screen/Expenditure/ExpenditureScreen.dart';
import 'package:ipms_app/presentation/Screen/HomePage/HomeScreenController.dart';
import 'package:ipms_app/presentation/Screen/Income/IncomeScreen.dart';
import 'package:ipms_app/presentation/Screen/LogOut/LogOutScreen.dart';
import 'package:ipms_app/presentation/Screen/MyProfile/MyProfileScreen.dart';
import 'package:ipms_app/presentation/Screen/Purchase/PurchaseScreen.dart';
import 'package:ipms_app/presentation/Screen/Sales/SalesScreen.dart';
import 'package:ipms_app/presentation/Screen/SubContact/SubContactScreen.dart';
import 'package:ipms_app/presentation/Screen/Vendor/VendorScreen.dart';
class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title:Text('Star Warz'),
      ),
      drawer: DrawerMenu(),
      body: Container(
        child: GetBuilder<HomeScreenController>(
          builder: (obj){
            if( obj.screeenId == 1){
              return MyProfileScreen();
            }
            if( obj.screeenId == 2){
              return CustomerScreen();
            }
            if( obj.screeenId == 3){
              return VendorScreen();
            }
            if( obj.screeenId == 4){
              return SubContractScreen();
            }
            if( obj.screeenId == 5){
              return EmployeeInfoScreen();
            }
            if( obj.screeenId == 6){
              return PurchaseScreen();
            }
            if( obj.screeenId == 7){
              return SalesScreen();
            }
            if( obj.screeenId == 8){
              return ExpenditureScreen();
            }
            if( obj.screeenId == 9){
              return IncomeScreen();
            }
            if( obj.screeenId == 10){
              return LogOutScreen();
            }



             return SafeArea(child: Container(
               height: Get.height,
               width: Get.width,
               color: Colors.blue,
               child: Text('Home'),
             )
             ) ;
          }
        ),
      ),

    );
  }
}
