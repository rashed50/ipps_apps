import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipms_app/presentation/Routes/AppRoutes.dart';
import 'package:ipms_app/presentation/Screen/HomePage/HomeScreenController.dart';

class LogInScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      body: SafeArea(
      child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment:CrossAxisAlignment.center ,
      children:<Widget> [
      Container(height: 35,
      width: Get.width,
      alignment: Alignment.center,
      child: Text('Please LogIn to Proceed',
      style: TextStyle(
      fontSize: 18,
      color: Colors.grey[400],
      fontWeight: FontWeight.w600,
      letterSpacing: 1,
      ),),
      ),
      Container(
      margin: EdgeInsets.fromLTRB(30, 5, 30, 5),
      height: 50,
      width: Get.width,
      color: Colors.grey[500],
      child: TextField(
      decoration: InputDecoration(
      fillColor: Colors.grey[500],
      focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.amber[400],width: 2)
      ),
      enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.deepOrange[400],width: 2)
      ),
      hintText: 'Input Your Username',
      labelStyle: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.white
      ),
      labelText: 'Username'
      ),
      ),
      ),
      Container(
      margin: EdgeInsets.fromLTRB(30, 5, 30, 5),
      height: 50,
      width: Get.width,
      color: Colors.grey[500],
      child: TextField(
      obscureText: true,
      decoration: InputDecoration(
      fillColor: Colors.grey[500],
      focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.amber[400],width: 2)
      ),
      enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.deepOrange[400],width: 2)
      ),
      hintText: 'Input Password',
      labelText: 'PASSWORD',
      labelStyle: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.white
      ),
      ),
      ),
      ),
      Container(child: RaisedButton.icon(
      onPressed: (){
        Get.toNamed(AppRoutes.HOME_SCREEN);
      },
      color:Colors.grey[800],splashColor:Colors.deepOrange,icon: Icon(Icons.login,color:Colors.amber[600]), label:Text('LogIn',
      style: TextStyle(
      fontSize: 18,
      color: Colors.grey[400],
      fontWeight: FontWeight.w800,
      letterSpacing: .5,
      ),), ),)
      ],
      ),
    ),
);
  }
}
