import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipms_app/presentation/Screen/MyProfile/MyProfileScreenController.dart';
class MyProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MyProfileScreenController>(
      builder: (_to){
     return Container(
          height: 500,
          width: 400,
          color: Colors.teal,
          child: Text(_to.companyProfileInfo != null ? _to.companyProfileInfo.comNameEnglish : 'Company Profile',
            style: TextStyle(
                fontSize: 30,
                color: Colors.white
            ),
          ),
        );
      },

    );
  }
}
