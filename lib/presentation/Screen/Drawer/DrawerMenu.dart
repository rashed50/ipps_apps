
import 'package:flutter/material.dart';
import 'package:get/get.dart';
//import 'package:ipms_app/presentation/Routes/AppRoutes.dart';
import 'package:ipms_app/presentation/Screen/HomePage/HomeScreenController.dart';
import 'package:ipms_app/core/widgets/AppColors.dart';

class DrawerMenu extends StatelessWidget {
  HomeScreenController obj = Get.find();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: AppColors.bodyBackgroundColor,
        width: Get.width / 1.8,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _drawerProfile(),
            Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: _drawerMenuButton()),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _drawerMenuButton() {
    List<String> menutTitle = [
      'My Profile',
      'Customer',
      'Purchase From',
      'Sub-Contact',
      'Employee Info',
      'Purchase',
      'Sales',
      'Expenditure',
      'Income',
      'Log Out'
    ];
    return menutTitle
        .map((aTitle) => Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(0, 0, 0, 1),
              padding: EdgeInsets.only(left: 10),
              color: Colors.white,
              child: ListTile(
                onTap: () {
                  if (aTitle == 'My Profile') {
                    obj.screeenId = 1;
                    obj.updateSelectedMenu();
                    Get.back();
                    //  Get.toNamed(AppRoutes.MYPROFILE_SCREEN);
                  } else if (aTitle == 'Customer') {
                    obj.screeenId = 2;
                    obj.updateSelectedMenu();
                    Get.back();
                    // Get.toNamed(AppRoutes.CUSTOMER_SCREEN);
                  } else if (aTitle == 'Purchase From') {
                    obj.screeenId = 3;
                    obj.updateSelectedMenu();
                    Get.back();
                    // Get.toNamed(AppRoutes.VENDOR_SCREEN);
                  } else if (aTitle == 'Sub-Contact') {
                    obj.screeenId = 4;
                    obj.updateSelectedMenu();
                    Get.back();
                  } else if (aTitle == 'Employee Info') {
                    obj.screeenId = 5;
                    obj.updateSelectedMenu();
                    Get.back();
                  } else if (aTitle == 'Purchase') {
                    obj.screeenId = 6;
                    obj.updateSelectedMenu();
                    Get.back();
                  } else if (aTitle == 'Sales') {
                    obj.screeenId = 7;
                    obj.updateSelectedMenu();
                    Get.back();
                  } else if (aTitle == 'Expenditure') {
                    obj.screeenId = 8;
                    obj.updateSelectedMenu();
                    Get.back();
                  } else if (aTitle == 'Income') {
                    obj.screeenId = 9;
                    obj.updateSelectedMenu();
                    Get.back();
                  } else if (aTitle == 'Log Out') {
                    obj.screeenId = 10;
                    obj.updateSelectedMenu();
                    Get.back();
                  }
                },
                leading: Icon(Icons.adb_rounded),
                title: Text(aTitle,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      fontSize: 14,
                    )),
              ),
            ))
        .toList();
  }

  Widget _drawerProfile() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
      color: Colors.grey[900],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            backgroundImage: NetworkImage(
                'https://images.unsplash.com/photo-1542744173-8e7e53415bb0?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'),
            radius: 60,
          ),
          Text(
            'TNT International Ltd.',
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.w600,
              letterSpacing: 1,
            ),
          ),
          Text(
            'Mirpur,Dhaka,Bangladesh.',
            style: TextStyle(
              fontSize: 12,
              color: Colors.grey[400],
              fontWeight: FontWeight.w600,
              letterSpacing: 1,
            ),
          ),
          Text(
            'ESTD-2015',
            style: TextStyle(
              fontSize: 12,
              color: Colors.grey[400],
              fontWeight: FontWeight.w600,
              letterSpacing: 1,
            ),
          ),
        ],
      ),
    );
  }
}
