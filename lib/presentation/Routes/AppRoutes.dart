import 'package:get/get.dart';
import 'package:ipms_app/presentation/Screen/Customer/AddCustomer/AddCustomerScreen.dart';
import 'package:ipms_app/presentation/Screen/Customer/CustomerScreen.dart';
import 'package:ipms_app/presentation/Screen/EmployeeInfo/EmployeeInfoScreen.dart';
import 'package:ipms_app/presentation/Screen/Expenditure/ExpenditureScreen.dart';
import 'package:ipms_app/presentation/Screen/HomePage/HomeScreen.dart';
import 'package:ipms_app/presentation/Screen/Income/IncomeScreen.dart';
import 'package:ipms_app/presentation/Screen/LogIn/LogInScreen.dart';
import 'package:ipms_app/presentation/Screen/LogOut/LogOutScreen.dart';
import 'package:ipms_app/presentation/Screen/MyProfile/MyProfileScreen.dart';
import 'package:ipms_app/presentation/Screen/Payment/PaymentScreen.dart';
import 'package:ipms_app/presentation/Screen/Purchase/PurchaseScreen.dart';
import 'package:ipms_app/presentation/Screen/Sales/SalesScreen.dart';
import 'package:ipms_app/presentation/Screen/SubContact/SubContactScreen.dart';
import 'package:ipms_app/presentation/Screen/SubContact/SubContractBooking/SubContractBooking.dart';
import 'package:ipms_app/presentation/Screen/SubContact/SubContractPayment/SubContractPaymentScreen.dart';
import 'package:ipms_app/presentation/Screen/SubContact/SubContractor/SubContractorScreen.dart';
import 'package:ipms_app/presentation/Screen/Vendor/VendorScreen.dart';


class AppRoutes {
static String CUSTOMER_SCREEN = 'CustomerScreen';
static String HOME_SCREEN = 'HomeScreen';
static String EXPENDITURE_SCREEN = 'ExpenditureScreen';
static String INCOME_SCREEN = 'IncomeScreen';
static String LOGOUT_SCREEN = 'LogOutScreen';
static String MYPROFILE_SCREEN = 'MyProfileScreen';
static String PAYMENT_SCREEN = 'PaymentScreen';
static String PURCHASE_SCREEN = 'PurchaseScreen';
static String SALES_SCREEN = 'SalesScreen';
static String SUBCONTACT_SCREEN = 'SubContactScreen';
static String VENDOR_SCREEN = 'VendorScreen';
static String LOGIN_SCREEN = 'LogInScreen';
static String EMPLOYEEINFO_SCREEN = 'EmployeeInfoScreen';
static String ADDCUSTOMER_SCREEN = 'AddCustomerScreen';
static String SUBCONTRACTOR_SCREEN = 'SubContractorScreen';
static String SUBCONTRACTPAYMENT_SCREEN = 'SubContractPaymentScreen';
static String SUBCONTRACTBOOKING_SCREEN = 'SubContractBookingScreen';


 static List<GetPage> appRoutesList(){
    return [
      GetPage(name: CUSTOMER_SCREEN, page:()=>CustomerScreen()),
      GetPage(name: HOME_SCREEN, page:()=> HomeScreen()),
      GetPage(name: EXPENDITURE_SCREEN, page:()=> ExpenditureScreen ()),
      GetPage(name: INCOME_SCREEN, page:()=>  IncomeScreen()),
      GetPage(name: LOGOUT_SCREEN, page: ()=>LogOutScreen()),
      GetPage(name: MYPROFILE_SCREEN, page: ()=>MyProfileScreen()),
      GetPage(name: PAYMENT_SCREEN, page: ()=>PaymentScreen()),
      GetPage(name: PURCHASE_SCREEN, page:()=> PurchaseScreen()),
      GetPage(name: SALES_SCREEN, page: ()=>SalesScreen()),
      GetPage(name: SUBCONTACT_SCREEN, page:()=> SubContractScreen()),
      GetPage(name: VENDOR_SCREEN, page:()=> VendorScreen()),
      GetPage(name: LOGIN_SCREEN, page: ()=>LogInScreen()),
      GetPage(name: EMPLOYEEINFO_SCREEN, page: ()=> EmployeeInfoScreen()),
      GetPage(name: ADDCUSTOMER_SCREEN, page: ()=>AddCustomerScreen()),
      GetPage(name: SUBCONTRACTOR_SCREEN, page: ()=>SubContractorScreeen()),
      GetPage(name: SUBCONTRACTPAYMENT_SCREEN, page: () => SubContractPaymentScreen()),
      GetPage(name: SUBCONTRACTBOOKING_SCREEN, page: ()=>SubContractBookingScreen())
     // GetPage(name: , page:()=>  ()),

    ];
  }
}