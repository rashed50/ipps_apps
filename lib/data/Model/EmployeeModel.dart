import 'dart:core';

class EmployeeModel{
  String  employeeId,employeeNameBengla,employeeNameEnglish,employeeFatherName,employeeEmail,employeeMobileNo1,employeeMobileNo2,
      employeePhoto,employeeDescription,employeeNid;
  int countryId, divisionId,districtId,upazilaId,designationId,factoryId,factoryUnitId,employeeTypeId;
  double salary = 0.0;


  EmployeeModel.fromJson(Map<String, dynamic> json) {
    print('calling for making model ' + json['comNameEnglish']);
    this.employeeNameBengla = json['employeeNameBengla'];
    this.employeeNameEnglish = json['employeeNameEnglish'];
    this.employeeFatherName = json['employeeFatherName'];
    this.employeeEmail = json['employeeEmail'];
    this.employeeMobileNo1 = json['employeeMobileNo1'];
    this.employeeMobileNo2 = json['employeeMobileNo2'];
    this.employeePhoto = json['employeePhoto'];
    this.employeeDescription = json['employeeDescription'];
    this.employeeNid = json['employeeNid'];
    this.countryId = json['countryId'];
    this.divisionId = json['divisionId'];
    this.districtId = json['districtId'];
    this.upazilaId = json['upazilaId'];
    this.designationId = json['designationId'];
    this.factoryId = json['factoryId'];
    this.factoryUnitId = json['factoryUnitId'];
    this.employeeTypeId = json['employeeTypeId'];
    this.salary = json['salary'];
  }



  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['employeeNameBengla'] = this.employeeNameBengla;
    data['employeeNameEnglish'] = this.employeeNameEnglish;
    data['employeeFatherName'] = this.employeeFatherName;
    data['employeeEmail'] = this.employeeEmail;
    data['employeeMobileNo1'] = this.employeeMobileNo1;
    data['employeeMobileNo2'] = this.employeeMobileNo2;
    data['employeePhoto'] = this.employeePhoto;
    data['employeeDescription'] = this.employeeDescription;
    data['employeeNid'] = this.employeeNid;
    data['countryId'] = this.countryId.toString();
    data['divisionId'] = this.divisionId.toString();
    data['districtId'] = this.districtId.toString();
    data['upazilaId'] = this.upazilaId.toString();
    data['designationId'] = this.designationId.toString();
    data['factoryId'] = this.factoryId.toString();
    data['factoryUnitId'] = this.factoryUnitId.toString();
    data['employeeTypeId'] = this.employeeTypeId.toString();
    data['salary'] = this.salary.toString();
    return data;
  }
}


