

import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:ipms_app/data/Model/EmployeeModel.dart';
import 'package:ipms_app/data/Repository/Error/failures.dart';
import 'package:ipms_app/data/Repository/Remote/EmployeeRemoteRepository.dart';

class EmployeeRepository{

 Future<bool> saveNewEmployeeInformation(EmployeeModel employeeModel) async{
    EmployeeRemoteRepository empRemoteRepo = new EmployeeRemoteRepository();
   Either<dynamic,Failure> apiResponse = await empRemoteRepo.insertNewEmployeeInformation(employeeModel.toJson());
   apiResponse.fold((l){
     Map<String,dynamic> jsonresponse = jsonDecode(l.body);
         if(jsonresponse['status_code'] == 200){
           return true;
         }
   }, (r){

   });

    return false;
  }

}