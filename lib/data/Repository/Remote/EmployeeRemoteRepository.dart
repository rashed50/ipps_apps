import 'package:dartz/dartz.dart';
import 'package:ipms_app/data/Repository/Error/failures.dart';
import 'package:ipms_app/data/Repository/core/ApiClient.dart';
import 'package:ipms_app/data/Repository/core/ApiUrls.dart';

class EmployeeRemoteRepository{

  insertNewEmployeeInformation(Map<String,dynamic> json) async{
    try{
      // if(await NetworkInfoController.to.isConnected)
      //  {
      print('Phone no of the employee is ');
      dynamic response = await ApiClient.Request(
          url: ApiUrls.get_company_profile,
          enableHeader: false,
          body: json,
          method: Method.POST
      );
      return Left(response);
      //  }

    }
    catch(err) {
      print("Error : ${err}");
      return Right(ServerFailure());
    }
  }
}