

import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:ipms_app/core/Network/NetworkInfo.dart';
import 'package:ipms_app/data/Repository/Error/failures.dart';
import 'package:ipms_app/data/Repository/core/ApiClient.dart';
import 'package:ipms_app/data/Repository/core/ApiUrls.dart';

class CompanyProfileRemoteRepository{

  Future<Either<dynamic,Failure>> getCompanyProfileInformation() async{
    try{
     // if(await NetworkInfoController.to.isConnected)
     //  {
        print('Phone no of the employee is ');
        dynamic response = await ApiClient.Request(
            url: ApiUrls.get_company_profile, //ApiUrls.phone_no_verificaton,//
            enableHeader: false,
            body: 'jhhj',
            method: Method.GET
        );
     return Left(response);
    //  }

    }
    catch(err) {
      print("Error : ${err}");
      return Right(ServerFailure());
    }
  }

  Future<Either<dynamic,Failure>> insertCompanyAssetInformation(Map<String,dynamic> json) async{
    try{
      // if(await NetworkInfoController.to.isConnected)
      //  {
      print('Phone no of the employee is ');
      dynamic response = await ApiClient.Request(
          url: ApiUrls.save_Company_Asset_Information,
          enableHeader: false,
          body: jsonEncode(json),
          method: Method.POST
      );
      return Left(response);
      //  }

    }
    catch(err) {
      print("Error : ${err}");
      return Right(ServerFailure());
    }
  }
}