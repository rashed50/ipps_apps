

class ApiUrls{
  static String base_url = "http://ipmsappapi.starhairbd.com/api/";

  //Authentication
  static String get_company_profile = "${base_url}get-company-profile";

  // Company Asset information
  static String save_Company_Asset_Information = "${base_url}save-assetinfo";
}